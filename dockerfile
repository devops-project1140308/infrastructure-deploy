# Use a specific version of Alpine for reproducibility
FROM alpine:latest

# Install system dependencies for Terraform
RUN apk add --update --no-cache curl openssh-client python3 py3-pip unzip gcc musl-dev python3-dev libffi-dev openssl-dev && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir cffi

# Install Terraform
ENV TERRAFORM_VERSION=1.7.4
RUN curl -O https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
    rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Set up working directory
WORKDIR /workspace

# Clean APK cache to reduce image size
RUN rm -rf /var/cache/apk/*
