variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "nb_public_subnets" {
  description = "Number of public subnets (max 2)"
  type        = number
  default     = 1
  validation {
    condition     = var.nb_public_subnets <= 2
    error_message = "Number of public subnets cannot exceed 2"
  }
}

variable "nb_private_subnets" {
  description = "Number of private subnets (max 2)"
  type        = number
  default     = 1
  validation {
    condition     = var.nb_private_subnets <= 2
    error_message = "Number of private subnets cannot exceed 2"
  }
}

variable "ami-tag" {
  description = "AMI tag"
  type        = string
  default     = "latest"
}

variable "ingress_port" {
  description = "The port number for the ingress rule"
  type        = number
  default     = 8080
}

