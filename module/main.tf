terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0.0"
    }
  }

  backend "http" {
    address          = "https://gitlab.com/api/v4/projects/55320236/terraform/state/tfstate"
    lock_address     = "https://gitlab.com/api/v4/projects/55320236/terraform/state/tfstate/lock"
    unlock_address   = "https://gitlab.com/api/v4/projects/55320236/terraform/state/tfstate/lock"
    username         = "Alenas-c"
    password         = "glpat-vzGNJXBdWm49_7Us6wk-"
    lock_method      = "POST"
    unlock_method    = "DELETE"
    retry_wait_min   = 5
  }
}

provider "aws" {
  region = var.region
}
resource "aws_vpc" "web_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "${var.ami-tag}-VPC"
  }
}

resource "aws_subnet" "public_subnets" {
  count             = var.nb_public_subnets
  vpc_id            = aws_vpc.web_vpc.id
  cidr_block        = element(["10.0.1.0/24", "10.0.2.0/24"], count.index)
  availability_zone = element(["us-east-1a", "us-east-1b"], count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.ami-tag}-PublicSubnet-${count.index + 1}"
  }
}

resource "aws_subnet" "private_subnets" {
  count             = var.nb_private_subnets
  vpc_id            = aws_vpc.web_vpc.id
  cidr_block        = element(["10.0.3.0/24", "10.0.4.0/24"], count.index)
  availability_zone = element(["us-east-1a", "us-east-1b"], count.index)
  tags = {
    Name = "${var.ami-tag}-PrivateSubnet-${count.index + 1}"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.web_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "${var.ami-tag}-PublicRouteTable"
  }
}

resource "aws_route_table_association" "public_subnet_associations" {
  count             = var.nb_public_subnets
  subnet_id         = aws_subnet.public_subnets[count.index].id
  route_table_id    = aws_route_table.public_route_table.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.web_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.ngw.id
  }

  tags = {
    Name = "${var.ami-tag}-PrivateRouteTable"
  }
}

resource "aws_route_table_association" "private_subnet_associations" {
  count             = var.nb_private_subnets
  subnet_id         = aws_subnet.private_subnets[count.index].id
  route_table_id    = aws_route_table.private_route_table.id
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.web_vpc.id
  tags = {
    Name = "${var.ami-tag}-InternetGateway"
  }
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.ngw.id
  subnet_id     = element(aws_subnet.public_subnets[*].id, 0)
  tags = {
    Name = "${var.ami-tag}-NatGateway"
  }
}

resource "aws_security_group" "public" {
  name        = "${var.ami-tag}-SecurityGroup"
  vpc_id      = aws_vpc.web_vpc.id

  ingress {
    from_port   = var.ingress_port
    to_port     = var.ingress_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.ami-tag}-SecurityGroup"
  }
}


resource "aws_eip" "ngw" {
  domain = "vpc"
  tags = {
    Name = "${var.ami-tag}-EIP"
  }
}
