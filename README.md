# Terraform Infrastructure Deployment

This repository contains the Terraform configuration files for provisioning cloud resources. The infrastructure is managed as code and deployed via a GitLab CI/CD pipeline.

## Project Structure

- `module/`
  - `main.tf` - The primary configuration file for Terraform that defines the resources to be created.
  - `variables.tf` - Definition of variables used within the Terraform configuration.
  - `outputs.tf` - Output values after Terraform execution.
- `.gitlab-ci.yml` - The GitLab CI/CD pipeline configuration for automating the deployment process.
- `README.md` - Documentation for the project (this file).

## Prerequisites

- GitLab Runner with the shell executor.
- Terraform installed on the runner (The CI pipeline will install Terraform if it's not present).

## Usage

### Configuration

Set the following variables in the GitLab project's CI/CD settings:

- `PUBLIC_SUBNETS`: The number of public subnets to create.
- `PRIVATE_SUBNETS`: The number of private subnets to create.

These variables can also be set in the `.gitlab-ci.yml` file, but it's recommended to use the project settings for ease of management and security.

### Pipeline Stages

The CI/CD pipeline consists of the following stages:

- `init`: Install Terraform if it's not already installed on the runner.
- `plan`: Execute `terraform plan` to preview the infrastructure changes.
- `apply`: Manually trigger `terraform apply` to provision the resources as defined in the Terraform configuration.

### Running the Pipeline

To run the pipeline, push your changes to the `main` branch or any other branch you've configured the pipeline to listen to. The `plan` stage will execute automatically, and the `apply` stage requires a manual trigger to proceed with the changes.

## Contributing

To contribute to this project, please create a branch and submit a merge request for review with your proposed changes.
